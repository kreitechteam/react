#Template for React-Redux Project.

## Demo

A running demo for the project can be seen [here](https://project-base.kreitech.io/)

## Redux
Redux is integrated to the template. Actions should be placed in the
src/actions folder, defining all types in a separate file as consts and import them.

Reducers should be placed in the src/reducers folder. Remember to not mutate state.

Store is created in src/Root.js, which is imported in src/App.js. In src/App.js you can
also set the initial state of the store.

Redux thunk middleware has been installed for async action creators.

A custom middleware to handle localStorage has been created, in src/middlewares folder.
If you wish to modify localStorage with new actions, import and handle them in src/middlewares/localStorage.js

## Routes
All routes are set in the src/App.js folder, using react-router-dom.

## API requests

[Axios](https://github.com/axios/axios) is used for API requests. A custom instance can be found in the src/services folder.
An interceptor for unauthorized requests (response status 401) has been included. The interceptor
is added in src/Root.js.

API url is located in .env.production and .env.development files.

## OAuth

Facebook and Google authentication services are included.
Application ids are located in .env.production and .env.development files.
For more information see src/components/oauth, where components for OAuth
are located.

To create a new Google project, refer to [Google Sign In](https://developers.google.com/identity/sign-in/web/sign-in). For Facebook, go to [Facebook Developers](https://developers.facebook.com/).

In order to sign in with Facebook in production, you have to switch the app status from development to public, in the app configuration.
## Testing

[Jest](https://jestjs.io/), [enzyme](https://github.com/airbnb/enzyme) and [moxios](https://github.com/axios/moxios) are used for testing.
All files ending with .test.js are considered tests by Jest. However, there is a __tests__ folder in src/components, src/actions,
src/middlewares, where corresponding tests should be placed.

[Moxios](https://github.com/axios/moxios) is the Axios library to mock requests, enzyme is used to render components.
Snapshot testing is used for components.

A code coverage report can be found in the base folder.

Remember to wrap components with the Root component if testing a redux connected
component.

Global test configuration is located in src/setupTests.js.
If you update react version, you must update the package enzyme-adapter-react-xx,
where xx is the react version. At the moment enzyme-adapter-react-16 is being used,
and edit src/setupTests.js to use the new version of the adapter.

Use
```
npm test
```
to run tests.

### Code coverage
For code coverage run:
```
npm test -- --coverage
```
Results will appear in terminal and in a folder called coverage, in the project root folder

### Example of snapshot testing
```
it('renders correctly', () => {
  const wrapper = shallow(<Root><Signout /></Root>);
  expect(toJson(wrapper)).toMatchSnapshot();
});
```

A snapshot will be created the first time the test is run. If the component is modified
and snapshot fails, the option to update it will appear. Update the snapshot if a change
was made to the component or check what made the test fail. Snapshots files have to be
commited.

## Sass

This template  uses scss for styles. All style files should be
placed inside src/sass, in the corresponding folder and imported in
src/main.scss. At execution or build all sass files are converted to a single
css, which is already imported in components/App.js.

## eslint

Eslint with [airbnb style guide](https://github.com/airbnb/javascript) has been configured.
Some rules have been deactivated and can be seen in the configuration file, .eslintrc.

## PropTypes

[PropTypes](https://reactjs.org/docs/typechecking-with-proptypes.html) for type checking of props
are required. For example:

```
import React, { Component } from 'react';
import PropTypes from 'prop-types';
class Header extends Component {
  ...
};

Header.propTypes = {
  name: propType.string.isRequired,
}
```

To see all types visit [PropTypes](https://reactjs.org/docs/typechecking-with-proptypes.html)


## High order components

A high order component to access authorized components and another one
to avoid logged in users access some functionality (for instance login)
can be found in the src/hoc folder.

In order to use them, export the component wrapped by the HOC component.
For example:

```
export default requireAuth(ComponentWhichRequiresAuth));
```

If the exported component is rendered, and the user is not logged in, it will be redirected to login page

or if it is a connected component:

```
export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(ComponentWhichRequiresAuth));
```

You can also use the redux compose function, to nest high order components in
an easier to read syntax.

```
export default compose(
  reduxForm({ validate, form: 'ComponentWhichRequiresAuthForm' }),
  connect(mapStateToProps, mapDispatchToProps),
  unauthorizedOnly,
)(ComponentWhichRequiresAuth);
```

In the last example, the component is connected to redux store, reduxForm and it
also needs authentication.

## npm scripts

```
npm run lint
```
Runs eslint and outputs the warning and errors.

```
npm run lint:fix
```
Runs eslint, fixes errors and warnings that can be automatically fixed and
outputs results.

```
npm run test
```

Run tests and output results.

```
npm run build-css
```
Compile sass files to a single css file.

```
npm run watch-css
```

Compile sass files in daemon mode, watching for file changes.

```
npm run start-js
```
create-react-app start script.

```
npm run build-js
```

create-react-app build script.

```
npm run start
```

Runs watch-css and start-js

```
npm run build
```

Runs linter, build-css and build-js. Build cannot be created if eslint finds
errors that can't be automatically fixed.


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). For more information, see their documentation.