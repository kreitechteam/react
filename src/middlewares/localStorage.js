import { LOGIN, LOGOUT, AUTH_ERROR } from '../actions/types';

// middleware to handle all actions that modify localStorage.
// this middleware might be modified to add new actions
// check for action type and do something according to that type.

export default () => next => (action) => {
  if (action.type === LOGIN) {
    localStorage.setItem('token', action.payload.token);
    localStorage.setItem('name', action.payload.name);
    localStorage.setItem('role', action.payload.role);
    localStorage.setItem('email', action.payload.email);
    localStorage.setItem('id', action.payload.id);
  }
  if (action.type === LOGOUT || action.type === AUTH_ERROR) {
    localStorage.removeItem('token');
    localStorage.removeItem('name');
    localStorage.removeItem('role');
    localStorage.removeItem('email');
    localStorage.removeItem('id');
  }
  next(action);
};
