import localStorageMiddleware from '../localStorage';
import { LOGIN, LOGOUT } from '../../actions/types';


const token = 'LOGIN TOKEN';

const create = () => {
  const store = {
    getState: jest.fn(() => ({})),
    dispatch: jest.fn(),
  };

  const next = jest.fn();
  const invoke = action => localStorageMiddleware()(next)(action);
  return { store, next, invoke };
};

it('sets token on login', () => {
  const { invoke, next } = create();
  const action = { type: LOGIN, payload: { token } };
  invoke(action);
  expect(next).toHaveBeenCalledWith(action);

  expect(localStorage.setItem.mock.calls.length).toBe(5);
  expect(localStorage.setItem.mock.calls[0]).toEqual(['token', token]);
});

it('removes token on logout', () => {
  const { invoke, next } = create();
  const action = { type: LOGOUT };
  invoke(action);
  expect(next).toHaveBeenCalledWith(action);

  expect(localStorage.removeItem.mock.calls.length).toBe(5);
  expect(localStorage.removeItem.mock.calls[0]).toEqual(['token']);
});
