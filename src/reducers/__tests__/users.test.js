import userReducer from '../users';
import {
  GET_USERS, GET_USER, USERS_ERROR, UPDATE_USER, RESET_USERS,
} from '../../actions/types';

it('should handle GET_USERS', () => {
  const users = [];
  const usersAction = {
    type: GET_USERS,
    payload: { data: users, has_more: true },
  };
  expect(userReducer({ users: [] }, usersAction)).toEqual({ users, hasMore: true });
});

it('should handle GET_USER', () => {
  const user = {};
  const userAction = {
    type: GET_USER,
    payload: user,
  };
  expect(userReducer({}, userAction)).toEqual({ user });
});

it('should handle UPDATE_USER', () => {
  const updatedUser = { name: 'user', email: 'useremail' };
  const updateAction = {
    type: UPDATE_USER,
    payload: updatedUser,
  };
  expect(userReducer({ user: { _id: 'userid' } }, updateAction)).toEqual({ user: { ...updatedUser, _id: 'userid' }, message: 'User updated' });
});

it('should handle USER_ERROR', () => {
  const userAction = {
    type: USERS_ERROR,
  };
  expect(userReducer({}, userAction)).toEqual({});
});

it('should handle RESET_USERS', () => {
  const userAction = {
    type: RESET_USERS,
  };
  expect(userReducer({}, userAction)).toEqual({ users: [] });
});
