import authReducer, { INITIAL_STATE } from '../auth';
import {
  AUTH_ERROR, LOGIN, LOGOUT, CHANGE_AUTH_MESSAGE, UPDATE_USER,
} from '../../actions/types';

it('should handle AUTH_ERROR', () => {
  const errorAction = {
    type: AUTH_ERROR,
    payload: 'error',
  };
  expect(authReducer({}, errorAction)).toEqual({ ...INITIAL_STATE, error: 'error' });
});

it('should handle LOGIN', () => {
  const payload = { email: 'email', name: 'name', role: 'role' };
  const loginAction = {
    type: LOGIN,
    payload: { ...payload, token: 'token' },
  };
  expect(authReducer({}, loginAction)).toEqual({ authenticated: 'token', error: '', ...payload });
});

it('should handle LOGOUT', () => {
  const logoutAction = {
    type: LOGOUT,
  };
  expect(authReducer({}, logoutAction)).toEqual(INITIAL_STATE);
});

it('should handle CHANGE_AUTH_MESSAGE', () => {
  const message = 'message';
  const changeAuthMessageAction = {
    type: CHANGE_AUTH_MESSAGE,
    payload: message,
  };
  expect(authReducer({}, changeAuthMessageAction)).toEqual({ message, error: '' });
});

it('should handle UPDATE_USER', () => {
  const id = 'userid';
  const payload = { email: 'email', role: 'role' };
  const updateUserAction = {
    type: UPDATE_USER,
    payload: { ...payload, fullName: 'newname', id },
  };
  expect(authReducer({ id }, updateUserAction)).toEqual({ id, ...payload, name: 'newname' });
});
