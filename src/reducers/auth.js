import {
  AUTH_ERROR, LOGIN, LOGOUT, UPDATE_USER, CHANGE_AUTH_MESSAGE, AUTH_RESET_MESSAGES,
} from '../actions/types';

// Authenticated should contain the JWT token

export const INITIAL_STATE = {
  error: '', authenticated: '', name: '', role: '', email: '', id: '', message: '',
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOGIN: {
      const {
        token, email, name, role, id,
      } = action.payload;
      return {
        ...state, authenticated: token, error: '', email, name, role, id,
      };
    }
    case LOGOUT:
      return INITIAL_STATE;
    case AUTH_RESET_MESSAGES:
      return { ...state, error: '', message: '' };
    case AUTH_ERROR:
      return { ...INITIAL_STATE, error: action.payload };
    case CHANGE_AUTH_MESSAGE:
      return { ...state, message: action.payload, error: '' };
    case UPDATE_USER: {
      const {
        email, fullName, role, id,
      } = action.payload;
      if (id === state.id) {
        return {
          ...state, email, name: fullName, role,
        };
      }
      return state;
    }
    default:
      return state;
  }
}
