import {
  GET_USERS, GET_USER, USERS_ERROR, UPDATE_USER, RESET_USERS, RESET_MESSAGE_USERS,
} from '../actions/types';

const INITIAL_STATE = {
  user: {}, users: [], message: '', hasMore: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: [...state.users, ...action.payload.data],
        hasMore: action.payload.has_more,
      };
    case GET_USER:
      return { ...state, user: action.payload };
    case UPDATE_USER:
      return { ...state, message: 'User updated', user: { ...action.payload, _id: state.user._id } };
    case RESET_USERS:
      return { ...state, users: [] };
    case RESET_MESSAGE_USERS:
      return { ...state, message: '' };
    case USERS_ERROR:
      return state;
    default:
      return state;
  }
}
