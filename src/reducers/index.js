import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import authReducer from './auth';
import usersReducer from './users';

export default combineReducers({
  auth: authReducer,
  users: usersReducer,
  form: formReducer,
});
