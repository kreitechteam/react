import axios from 'axios';
import { AUTH_ERROR } from '../actions/types';

// Edit this file to add custom headers to application API requests
// or to change the authorization header

const API_URL = process.env.REACT_APP_API_URL;

class ApiRequest {
  constructor() {
    this.instance = axios.create({
      baseURL: API_URL,
    });
  }

  addUnauthorizedInterceptor(dispatch) {
    this.instance.interceptors.response.use(response => response, (error) => {
      if (error.response.status === 401) {
        console.error('401 Unauthorized'); /* eslint-disable-line no-console */
        dispatch({ type: AUTH_ERROR, payload: 'Authentication error' });
      }
      return Promise.reject(error);
    });
  }

  addNetworkErrorInterceptor() {
    this.instance.interceptors.response.use(response => response, (error) => {
      if (!error.response) {
        console.error('Network error'); /* eslint-disable-line no-console */
        // dispatch({ type: AUTH_ERROR, payload: 'Authentication error' });
        return Promise.reject({ response: { data: { error: 'Network error' } } }); /* eslint-disable-line prefer-promise-reject-errors */
      }
      return Promise.reject(error);
    });
  }

  getAuth() {
    const token = localStorage.getItem('token');
    let headers = {};

    if (token) {
      headers = { Authorization: token };
    }
    return headers;
  }


  get(url, params) {
    const headers = this.getAuth();
    return this.instance.get(url, { params, headers });
  }

  post(url, body) {
    const headers = this.getAuth();
    return this.instance.post(url, body, headers);
  }

  put(url, body) {
    const headers = this.getAuth();
    return this.instance.put(url, body, { headers });
  }

  patch(url, body) {
    const headers = this.getAuth();
    return this.instance.patch(url, body, headers);
  }

  delete(url, body) {
    const headers = this.getAuth();
    return this.instance.delete(url, body, headers);
  }
}

const apiInstance = new ApiRequest();

export default apiInstance;
