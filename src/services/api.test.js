import moxios from 'moxios';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import api from './api';


const mockStore = configureStore([thunk]);
let store;

beforeEach(() => {
  moxios.install(api.instance);
  store = mockStore();
  api.addUnauthorizedInterceptor(store.dispatch);
});

afterEach(() => {
  moxios.uninstall(api.instance);
});

it('should intercept 401', (done) => {
  api.post('/').catch(jest.fn());
  moxios.wait(() => {
    const request = moxios.requests.mostRecent();
    request.respondWith({
      status: 401,
    }).then(() => {
      expect(console.error).toHaveBeenCalledWith('401 Unauthorized'); /* eslint-disable-line no-console */
      done();
    });
  });
});
