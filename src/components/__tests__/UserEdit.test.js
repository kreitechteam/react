import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import UserEdit from '../UserEdit';
import Root from '../Root';

let wrapper;
const match = { params: { id: 'userid' } };
const initialState = { auth: { authenticated: 'token' } };
const history = { push: jest.fn() };

beforeEach(() => {
  wrapper = mount(
    <Root>
      <UserEdit
        match={match}
        initialState={initialState}
        history={history}
      />
    </Root>,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('shows error if username is not present', () => {
  const username = wrapper.find('input').first();
  username.simulate('blur');
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('shows error if email is not present', () => {
  const email = wrapper.find('input').at(2);
  email.simulate('blur');
  expect(toJson(wrapper)).toMatchSnapshot();
});
