import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { MemoryRouter } from 'react-router-dom';
import Signup from '../Signup';
import Root from '../Root';


let wrapper;

beforeEach(() => {
  wrapper = mount(
    <MemoryRouter initialEntries={[{ pathname: '/', key: 'testKey' }]}>
      <Root>
        <Signup />
      </Root>
    </MemoryRouter>,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('shows error if username is not present', () => {
  const username = wrapper.find('input').first();
  username.simulate('blur');
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('shows error if password is not present', () => {
  const password = wrapper.find('input').last();
  password.simulate('blur');
  expect(toJson(wrapper)).toMatchSnapshot();
});
