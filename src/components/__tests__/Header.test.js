import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { MemoryRouter } from 'react-router-dom';
import { HeaderWithStyles as Header } from '../Header';


const history = { push: jest.fn() };

let wrapper;

beforeEach(() => {
  wrapper = mount(
    <MemoryRouter initialEntries={[{ pathname: '/', key: 'testKey' }]}>
      <Header history={history} />
    </MemoryRouter>,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

/* it('goes to edit user', () => {
  const row = wrapper.find('tr').last();
  row.simulate('click');
  expect(history.push.mock.calls.length).toBe(1);
}); */
