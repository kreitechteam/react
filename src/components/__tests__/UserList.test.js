import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { MemoryRouter } from 'react-router-dom';
import { UserList } from '../UserList';


const history = { push: jest.fn() };
const getUsers = jest.fn();

const user1 = {
  role: 'user', _id: 'user_id1', email: 'user1@email.com', fullName: 'user1',
};
const user2 = {
  role: 'user', _id: 'user_id2', email: 'user2@email.com', fullName: 'user2',
};
const users = [user1, user2];

let wrapper;

beforeEach(() => {
  wrapper = mount(
    <MemoryRouter initialEntries={[{ pathname: '/', key: 'testKey' }]}>
      <UserList users={users} getUsers={getUsers} history={history} />
    </MemoryRouter>,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('calls getUsers', () => {
  expect(getUsers.mock.calls.length).toBe(1);
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('goes to edit user', () => {
  const row = wrapper.find('tr').last();
  row.simulate('click');
  expect(history.push.mock.calls.length).toBe(1);
});
