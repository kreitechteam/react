import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Signout } from '../Signout';


const history = { push: jest.fn() };
const signout = jest.fn();
const authenticated = 'yes';

let wrapper;

beforeEach(() => {
  wrapper = mount(<Signout authenticated={authenticated} signout={signout} history={history} />);
});

afterEach(() => {
  wrapper.unmount();
});

it('calls signout', () => {
  expect(signout.mock.calls.length).toBe(1);
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('redirects to login page', () => {
  wrapper.setProps({ authenticated: undefined }, () => {
    expect(history.push.mock.calls.length).toBe(1);
  });
});
