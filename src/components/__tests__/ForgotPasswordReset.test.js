import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ForgotPasswordReset from '../ForgotPasswordReset';
import Root from '../Root';

let wrapper;
const match = { params: { token: 'token' } };

beforeEach(() => {
  wrapper = mount(
    <Root>
      <ForgotPasswordReset match={match} />
    </Root>,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('shows error if password is not present', () => {
  const password = wrapper.find('input').at(0);
  password.simulate('blur');
  expect(toJson(wrapper)).toMatchSnapshot();
});
