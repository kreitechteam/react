import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ForgotPasswordRequest from '../ForgotPasswordRequest';
import Root from '../Root';

let wrapper;

beforeEach(() => {
  wrapper = mount(
    <Root>
      <ForgotPasswordRequest />
    </Root>,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('shows error if email is not present', () => {
  const email = wrapper.find('input').at(0);
  email.simulate('blur');
  expect(toJson(wrapper)).toMatchSnapshot();
});
