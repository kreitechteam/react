import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ActivateAccount } from '../ActivateAccount';

const token = 'TOKEN';
const match = { params: { token } };

let resetAuthMessages;
let activateAccount;
let wrapper;

beforeEach(() => {
  resetAuthMessages = jest.fn();
  activateAccount = jest.fn();
  wrapper = mount(<ActivateAccount
    error="error"
    message="message"
    match={match}
    activateAccount={activateAccount}
    resetAuthMessages={resetAuthMessages}
  />);
});

afterEach(() => {
  wrapper.unmount();
});

it('renders correctly', () => {
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('resets auth message', () => {
  expect(resetAuthMessages.mock.calls.length).toBe(1);
});

it('activates account', () => {
  expect(activateAccount.mock.calls.length).toBe(1);
});

it('changes message', () => {
  wrapper.setProps({ message: 'new message' });
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('changes error', () => {
  wrapper.setProps({ error: 'new error' });
  expect(toJson(wrapper)).toMatchSnapshot();
});
