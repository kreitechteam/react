import React from 'react';
import {
  BrowserRouter, Route, Switch, withRouter,
} from 'react-router-dom';
import '../scss/main.css';
import Title from './Title';
import Root from './Root';
import Signup from './Signup';
import Signin from './Signin';
import Signout from './Signout';
import Header from './Header';
import Home from './Home';
import UserListCards from './UserListCards';
import UserList from './UserList';
import UserEdit from './UserEdit';
import ForgotPasswordRequest from './ForgotPasswordRequest';
import ForgotPasswordReset from './ForgotPasswordReset';
import ActivateAccount from './ActivateAccount';
import NotFound from './NotFound';

const INITIAL_STATE = {
  // read id
  auth: {
    error: '', id: localStorage.getItem('id'), authenticated: localStorage.getItem('token'), name: localStorage.getItem('name'), role: localStorage.getItem('role'), email: localStorage.getItem('email'), message: '',
  },
};

const RoutedHeader = withRouter(Header);

// Edit this file to add a new route
// or to add global components like a header or a footer

const App = () => (
  <Root initialState={INITIAL_STATE}>
    <BrowserRouter>
      <div>
        <RoutedHeader />
        <Switch>
          <Route path="/" exact render={props => <Title title="Home"><Home {...props} /></Title>} />
          <Route path="/login" exact render={props => <Title title="Login"><Signin {...props} /></Title>} />
          <Route path="/signup" exact render={props => <Title title="Signup"><Signup {...props} /></Title>} />
          <Route path="/signout" exact component={Signout} />
          <Route path="/activate/:token" exact render={props => <Title title="Activate Account"><ActivateAccount {...props} /></Title>} />
          <Route path="/forgot" exact render={props => <Title title="Forgot Password"><ForgotPasswordRequest {...props} /></Title>} />
          <Route path="/forgot/:token" exact render={props => <Title title="Forgot Password"><ForgotPasswordReset {...props} /></Title>} />
          <Route path="/users_cards" exact render={props => <Title title="User List"><UserListCards {...props} /></Title>} />
          <Route path="/users" exact render={props => <Title title="User List"><UserList {...props} /></Title>} />
          <Route path="/users/:id" exact render={props => <Title title="Edit User"><UserEdit {...props} /></Title>} />
          <Route path="*" render={props => <Title title="Page not found"><NotFound {...props} /></Title>} />
        </Switch>
      </div>
    </BrowserRouter>
  </Root>
);


export default App;
