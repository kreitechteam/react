import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { signin as signinAction, resetAuthMessages } from '../actions';
import FormField from './FormField';

class SigninForm extends Component {
  componentWillMount() {
    const { resetAuthMessages } = this.props;
    resetAuthMessages();
  }

  onSubmit(formProps) {
    const { signinAction, resetAuthMessages } = this.props;
    resetAuthMessages();
    return signinAction(formProps);
  }

  render() {
    const {
      handleSubmit, error, invalid, submitting, pristine,
    } = this.props;

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="form">
          <Field
            label="Email"
            name="email"
            type="text"
            component={FormField}
          />
          <Field
            label="Password"
            name="password"
            type="password"
            component={FormField}
          />
          <div className="form__forgot">
            <Link to="/forgot">Forgot account?</Link>
            <Link to="/signup">Create a new account</Link>
          </div>
          <div className="form__button">
            <div className="marginBottomMedium" style={{ height: '25px' }}>
              {error && <div className="erroredText">{ error }</div> }
            </div>
            <Button variant="contained" type="submit" color="primary" disabled={invalid || submitting || pristine}>
              Login
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

SigninForm.propTypes = {
  signinAction: PropTypes.func.isRequired,
  resetAuthMessages: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
};

function validate(values) {
  const errors = {};

  /* if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  } */

  if (!values.email) {
    errors.email = 'Enter a email';
  }

  if (!values.password) {
    errors.password = 'Enter a password';
  }

  return errors;
}

function mapStateToProps(state) {
  return { error: state.auth.error, authenticated: state.auth.authenticated };
}

export default compose(
  reduxForm({ validate, form: 'signin' }),
  connect(mapStateToProps, { signinAction, resetAuthMessages }),
)(SigninForm);
