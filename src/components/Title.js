import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const Title = ({ children, title }) => (
  <Fragment>
    <div style={{ padding: '2rem', textAlign: 'center' }}>
      <Typography variant="headline" gutterBottom>
        {title}
      </Typography>
    </div>
    {children}
  </Fragment>
);

Title.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.object.isRequired,
};

export default Title;
