import React from 'react';
import unauthorizedOnly from './hoc/unauthorizedOnly';
import SigninForm from './SigninForm';
import FacebookLogin from './oauth/Facebook';
import GoogleLogin from './oauth/Google';

const Signin = () => (
  <div>
    <SigninForm />
    <FacebookLogin />
    <GoogleLogin />
  </div>
);


export default unauthorizedOnly(Signin);
