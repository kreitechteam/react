import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { forgotPasswordRequest, resetAuthMessages } from '../actions';
import FormField from './FormField';

class ForgotPasswordRequest extends Component {
  componentWillMount() {
    const { resetAuthMessages } = this.props;
    resetAuthMessages();
  }

  onSubmit(formProps) {
    const { forgotPasswordRequest, resetAuthMessages } = this.props;
    resetAuthMessages();
    return forgotPasswordRequest(formProps);
  }

  renderText() {
    const { error, message } = this.props;
    return (
      <div className="marginBottomMedium" style={{ height: '25px' }}>
        {error && <div className="erroredText">{ error }</div> }
        {message && <div>{ message }</div>}
      </div>
    );
  }

  render() {
    const {
      handleSubmit, invalid, submitting, pristine,
    } = this.props;

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="form">
          <Field
            label="Email"
            name="email"
            type="text"
            component={FormField}
          />
          <div className="form__button">
            {this.renderText()}
            <Button variant="contained" type="submit" color="primary" disabled={invalid || submitting || pristine}>
              SUBMIT
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

ForgotPasswordRequest.propTypes = {
  forgotPasswordRequest: PropTypes.func.isRequired,
  resetAuthMessages: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
};

function validate(values) {
  const errors = {};

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.email) {
    errors.email = 'Enter a email';
  }

  return errors;
}

function mapStateToProps(state) {
  const { error, message } = state.auth;
  return { error, message };
}

export default compose(
  reduxForm({ validate, form: 'resetPasswordRequest' }),
  connect(mapStateToProps, { forgotPasswordRequest, resetAuthMessages }),
)(ForgotPasswordRequest);
