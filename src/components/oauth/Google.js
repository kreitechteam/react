import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Google from 'react-google-login';
import googleButton from './assets/google_button.svg';
import { googleSignIn } from '../../actions';
import './assets/google_button.css';

// Component for login using google.
// In order to use it, edit clientId with your clientId
// And change the googleSignIn action.
// Also, change onFailure prop

// To change style edit the render prop.
// Remember to add the onClick handler given
// in renderProps.onClick

// Then, import component in login

const GoogleLogin = ({ googleSignIn }) => (
  <Google
    clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
    onSuccess={googleSignIn}
    onFailure={() => {}}
    render={renderProps => (
      <div style={{ textAlign: 'center', marginTop: '1rem' }}>
        <Button onClick={renderProps.onClick} />
      </div>
    )}
  />
);

GoogleLogin.propTypes = {
  googleSignIn: PropTypes.func.isRequired,
};

const Button = ({ onClick }) => (
  <button type="button" className="googleLoginButton" onClick={onClick}>
    <div className="googleLoginButton__div">
      <div className="googleLoginButton__img__div"><img src={googleButton} className="googleLoginButton__img" alt="Google Sign in" /></div>
      <span className="googleLoginButton__span">Sign in with Google</span>
    </div>
  </button>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default connect(null, { googleSignIn })(GoogleLogin);
