import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Facebook from 'react-facebook-login/dist/facebook-login-render-props';
import facebookButton from './assets/facebook_button.svg';
import './assets/facebook_button.css';
import { facebookSignIn } from '../../actions';

// Component for login using facebook.
// In order to use it, edit appId with your appId
// And change the facebookSignIn action.

// To change style edit the render prop.
// Remember to add the onClick handler given
// in renderProps.onClick

// Then, import component in login

const FacebookLogin = ({ facebookSignIn }) => (
  <Facebook
    appId={process.env.REACT_APP_FACEBOOK_APP_ID}
    autoLoad={false}
    fields="name,email,picture"
    callback={facebookSignIn}
    render={renderProps => (
      <div style={{ textAlign: 'center', marginTop: '1rem' }}>
        <Button onClick={renderProps.onClick} />
      </div>
    )}
  />
);

FacebookLogin.propTypes = {
  facebookSignIn: PropTypes.func.isRequired,
};

const Button = ({ onClick }) => (
  <button type="button" className="facebookLoginButton" onClick={onClick}>
    <div className="facebookLoginButton__div">
      <div className="facebookLoginButton__img__div">

        <img src={facebookButton} className="facebookLoginButton__img" alt="Facebook Sign in" />
      </div>
      <span className="facebookLoginButton__span">Sign in with Facebook</span>
    </div>
  </button>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default connect(null, { facebookSignIn })(FacebookLogin);
