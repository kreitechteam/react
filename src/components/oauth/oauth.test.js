import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Root from '../Root';
import Google from './Google';
import Facebook from './Facebook';

it('renders correctly google', () => {
  const wrapper = mount(<Root><Google /></Root>);
  expect(toJson(wrapper)).toMatchSnapshot();
  wrapper.unmount();
});

it('renders correctly facebook', () => {
  const wrapper = mount(<Root><Facebook /></Root>);
  expect(toJson(wrapper)).toMatchSnapshot();
  wrapper.unmount();
});
