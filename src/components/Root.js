import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from '../reducers';
import localStorageMiddleware from '../middlewares/localStorage';
import ApiRequest from '../services/api';

const Root = ({ children, initialState = {} }) => {
  const middlewares = applyMiddleware(reduxThunk, localStorageMiddleware);
  const store = createStore(reducers, initialState, middlewares);
  ApiRequest.addNetworkErrorInterceptor();
  ApiRequest.addUnauthorizedInterceptor(store.dispatch);
  return (
    <Provider store={store}>
      {children}
    </Provider>
  );
};

Root.propTypes = {
  children: PropTypes.object.isRequired,
  initialState: PropTypes.object.isRequired,
};


export default Root;
