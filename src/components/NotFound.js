import React from 'react';

export default () => (
  <div style={{ padding: '2rem' }}>
      Requested page was not found
  </div>
);
