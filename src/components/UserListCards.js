import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import requireAuth from './hoc/requireAuth';
import { getUsers } from '../actions';

export class UserList extends Component {
  componentDidMount() {
    const { getUsers } = this.props;
    getUsers(1);
  }

  rowClick(id) {
    const { history } = this.props;
    history.push(`/users/${id}`);
  }

  renderUsers() {
    const { users } = this.props;
    return users.map(user => (
      <UserCard
        key={user.email}
        fullName={user.fullName}
        role={user.role}
        email={user.email}
        onClick={() => this.rowClick(user._id)}
      />
    ));
  }

  render() {
    const { getUsers, hasMore } = this.props;
    return (
      <div className="userlistcards">
        <InfiniteScroll
          pageStart={1}
          loadMore={getUsers}
          hasMore={hasMore}
          loader={<div className="userlistcards__progress"><CircularProgress size={50} /></div>}
        >
          {this.renderUsers()}
        </InfiniteScroll>
      </div>
    );
  }
}

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  history: PropTypes.object.isRequired,
  getUsers: PropTypes.func.isRequired,
  hasMore: PropTypes.bool.isRequired,
};

const UserCard = ({
  fullName, email, role, onClick,
}) => (
  <Card className="userlistcards__card">
    <CardContent>
      <Typography variant="headline" component="h2">
        {fullName}
      </Typography>
      <Typography color="textSecondary">
        {role}
      </Typography>
      <Typography component="p">
        {email}
      </Typography>
    </CardContent>
    <CardActions>
      <Button onClick={onClick} size="small">Edit</Button>
    </CardActions>
  </Card>
);

UserCard.defaultProps = {
  email: '',
  fullName: '',
};

UserCard.propTypes = {
  email: PropTypes.string,
  fullName: PropTypes.string,
  role: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return { users: state.users.users, hasMore: state.users.hasMore };
}
export default connect(mapStateToProps, { getUsers })(requireAuth(UserList));
