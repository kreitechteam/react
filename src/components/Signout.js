import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { signout } from '../actions';

export class Signout extends Component {
  componentWillMount() {
    const { signout } = this.props;
    signout();
  }

  componentDidUpdate() {
    const { authenticated, history } = this.props;
    if (!authenticated) {
      history.push('/login');
    }
  }

  render() {
    return (
      <Fragment />
    );
  }
}

Signout.propTypes = {
  signout: PropTypes.func.isRequired,
  authenticated: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return { authenticated: state.auth.authenticated };
}

export default connect(mapStateToProps, { signout })(Signout);
