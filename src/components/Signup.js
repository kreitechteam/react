import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { signup as signupAction, resetAuthMessages } from '../actions';
import unauthorizedOnly from './hoc/unauthorizedOnly';
import FormField from './FormField';

class Signup extends Component {
  componentWillMount() {
    const { resetAuthMessages } = this.props;
    resetAuthMessages();
  }

  onSubmit(formProps) {
    const { signupAction, resetAuthMessages } = this.props;
    resetAuthMessages();
    return signupAction(formProps);
  }

  renderText() {
    const { error, message } = this.props;
    return (
      <div className="marginBottomMedium" style={{ height: '25px' }}>
        {error && <div className="erroredText">{ error }</div> }
        {message && <div>{ message }</div>}
      </div>
    );
  }

  render() {
    const {
      handleSubmit, invalid, submitting, pristine,
    } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="form">
        <Field
          label="Name"
          name="name"
          type="text"
          component={FormField}
        />
        <Field
          label="Email"
          name="email"
          type="text"
          component={FormField}
        />
        <Field
          label="Password"
          name="password"
          type="password"
          component={FormField}
        />
        <div className="form__forgot text-center" style={{ display: 'block' }}>
          <Link to="/login">Sign in</Link>
        </div>
        <div className="form__button">
          { this.renderText() }
          <Button variant="contained" type="submit" color="primary" disabled={invalid || submitting || pristine}>
            Sign Up
          </Button>
        </div>
      </form>
    );
  }
}

Signup.defaultProps = {
  error: '',
  message: '',
};

Signup.propTypes = {
  signupAction: PropTypes.func.isRequired,
  resetAuthMessages: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  message: PropTypes.string,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
};

function validate(values) {
  const errors = {};

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.email) {
    errors.email = 'Enter a email';
  }

  if (!values.password) {
    errors.password = 'Enter a password';
  }

  if (!values.name) {
    errors.name = 'Enter a name';
  }

  return errors;
}

function mapStateToProps(state) {
  const { error, message } = state.auth;
  return { error, message };
}

export default compose(
  reduxForm({ validate, form: 'signup' }),
  connect(mapStateToProps, { signupAction, resetAuthMessages }),
  unauthorizedOnly,
)(Signup);
