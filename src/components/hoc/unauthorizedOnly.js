import React, { Component } from 'react';
import { connect } from 'react-redux';

// HOC to redirect logged users
// if they try to access only unauthorized components
// such as signin and signup

// component is similar to requireAuth, more comments
// can be found there

export default (ChildComponent) => {
  class ComposedComponent extends Component {
    componentDidMount() {
      this.shouldNavigateAway();
    }

    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    shouldNavigateAway() {
      const { history, auth } = this.props;
      if (auth.authenticated) {
        history.push('/');
      }
    }

    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { auth: state.auth };
  }

  return connect(mapStateToProps)(ComposedComponent);
};
