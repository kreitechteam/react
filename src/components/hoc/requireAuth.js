import React, { Component } from 'react';
import { connect } from 'react-redux';

// HOC to redirect not logged in users
// if they try to access authenticated components

// edit shouldNavigateAway to modify the action
// that should happen.

// edit component to change the way user authorization is controlled

export default (ChildComponent) => {
  class ComposedComponent extends Component {
    componentDidMount() {
      this.shouldNavigateAway();
    }

    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    shouldNavigateAway() {
      const { history, auth } = this.props;
      if (!auth.authenticated) {
        history.push('/login');
      }
    }

    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { auth: state.auth };
  }

  return connect(mapStateToProps)(ComposedComponent);
};
