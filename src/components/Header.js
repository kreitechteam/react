import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Hidden from '@material-ui/core/Hidden';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/SwipeableDrawer';
import MenuItem from '@material-ui/core/MenuItem';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import Divider from '@material-ui/core/Divider';
import MenuIcon from '@material-ui/icons/Menu';
import CreateIcon from '@material-ui/icons/Create';
import AccountCircle from '@material-ui/icons/AccountCircle';
import InboxIcon from '@material-ui/icons/Inbox';
import HomeIcon from '@material-ui/icons/Home';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  drawer: {
    width: '70vw',
    backgroundColor: theme.palette.background.paper,
  },
});

class Header extends Component {
  constructor() {
    super();
    this.state = {
      anchorEl: null,
      drawer: false,
    };
    this.handleMenu = this.handleMenu.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.goToProfile = this.goToProfile.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  goToProfile() {
    const { history, id } = this.props;
    history.push(`/users/${id}`);
  }

  handleMenu(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleClose() {
    this.setState({ anchorEl: null });
  }

  toggleDrawer() {
    const { drawer } = this.state;
    this.setState({ drawer: !drawer });
  }

  renderDrawerLinks() {
    const { authenticated, history } = this.props;
    if (authenticated) {
      return (
        <List component="nav">
          <ListItem button>
            <ListItemIcon>
              <AccountCircle />
            </ListItemIcon>
            <ListItemText primary="User List" onClick={() => history.push('/users')} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Home" onClick={() => history.push('/')} />
          </ListItem>
          <ListItem button onClick={() => history.push('/signout')}>
            <ListItemIcon>
              <CreateIcon />
            </ListItemIcon>
            <ListItemText primary="Log Out" />
          </ListItem>
        </List>
      );
    }
    return (
      <List component="nav">
        <ListItem button>
          <ListItemIcon>
            <AccountCircle />
          </ListItemIcon>
          <ListItemText primary="Sign Up" onClick={() => history.push('/signup')} />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Sign In" onClick={() => history.push('/login')} />
        </ListItem>
      </List>
    );
  }

  renderDrawerUserList() {
    const { name, authenticated } = this.props;
    if (authenticated) {
      return (
        <Fragment>
          <Divider />
          <List component="nav">
            <ListItem button>
              <ListItemIcon>
                <AccountCircle />
              </ListItemIcon>
              <ListItemText primary={name} onClick={this.handleClose} />
            </ListItem>
            <ListItem button onClick={this.goToProfile}>
              <ListItemIcon>
                <CreateIcon />
              </ListItemIcon>
              <ListItemText primary="Profile" />
            </ListItem>
          </List>
        </Fragment>
      );
    }
    return (<Fragment />);
  }

  renderDrawer() {
    const { classes } = this.props;
    const { drawer } = this.state;
    return (
      <Hidden smUp>
        <IconButton onClick={this.toggleDrawer} className={classes.menuButton} color="inherit" aria-label="Menu">
          <MenuIcon />
        </IconButton>
        <Drawer open={drawer} onClose={this.toggleDrawer} onOpen={this.toggleDrawer}>
          <div
            onClick={this.toggleDrawer}
            onKeyDown={this.toggleDrawer}
            className={classes.drawer}
            tabIndex={0}
            role="button"
          >
            {this.renderDrawerLinks()}
            {this.renderDrawerUserList()}
          </div>
        </Drawer>
      </Hidden>
    );
  }

  renderLinks() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const {
      authenticated, name, history,
    } = this.props;
    if (authenticated) {
      return (
        <Hidden xsDown>
          <div>
            <Button color="inherit" onClick={() => history.push('/')}>Home</Button>
            <div style={{ display: 'inline-block' }}>
              <Button onClick={this.handleMenu} color="inherit">
                <AccountCircle />
                <span style={{ marginLeft: '5px' }}>{name}</span>
              </Button>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={this.handleClose}
                onClick={this.handleClose}
              >
                <MenuItem onClick={this.goToProfile}>Profile</MenuItem>
                <MenuItem onClick={() => history.push('/users')}>User List</MenuItem>
                <MenuItem onClick={() => history.push('/signout')}>Sign out</MenuItem>
              </Menu>
            </div>
          </div>
        </Hidden>
      );
    }
    return (
      <Hidden smDown>
        <div>
          <Button color="inherit" onClick={() => history.push('/signup')}>Sign Up</Button>
          <Button color="inherit" onClick={() => history.push('/login')}>Sign In</Button>
        </div>
      </Hidden>
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            {this.renderDrawer()}
            <Typography variant="title" color="inherit" className={classes.flex}>
                React Redux
            </Typography>
            {this.renderLinks()}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Header.defaultProps = {
  name: '',
  authenticated: '',
  id: '',
};

Header.propTypes = {
  history: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  name: PropTypes.string,
  authenticated: PropTypes.string,
  id: PropTypes.string,
};

function mapStateToProps(state) {
  const {
    authenticated, name, email, role, id,
  } = state.auth;
  return {
    authenticated, name, email, role, id,
  };
}

export const HeaderWithStyles = withStyles(styles)(Header);

export default connect(mapStateToProps)(withStyles(styles)(Header));
