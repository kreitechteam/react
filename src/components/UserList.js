import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { getUsers } from '../actions';
import requireAuth from './hoc/requireAuth';


export class UserList extends Component {
  componentDidMount() {
    const { getUsers } = this.props;
    getUsers(1);
  }

  rowClick(id) {
    const { history } = this.props;
    history.push(`/users/${id}`);
  }

  renderUsers() {
    const { users } = this.props;
    return users.map(user => (
      <TableRow onClick={() => this.rowClick(user._id)} key={user._id}>
        <TableCell>{user.fullName}</TableCell>
        <TableCell>{user.role}</TableCell>
        <TableCell>{user.email}</TableCell>
      </TableRow>
    ));
  }

  render() {
    const { getUsers, hasMore } = this.props;
    return (
      <div className="userlist">
        <InfiniteScroll
          pageStart={1}
          loadMore={getUsers}
          hasMore={hasMore}
          loader={<div className="userlist__progress"><CircularProgress size={50} /></div>}
        >
          <Paper>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Role</TableCell>
                  <TableCell>Email</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.renderUsers()}
              </TableBody>
            </Table>
          </Paper>
        </InfiniteScroll>
      </div>
    );
  }
}

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  history: PropTypes.object.isRequired,
  getUsers: PropTypes.func.isRequired,
  hasMore: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return { users: state.users.users, hasMore: state.users.hasMore };
}
export default connect(mapStateToProps, { getUsers })(requireAuth(UserList));
