import React from 'react';
import TextField from '@material-ui/core/TextField';

export default (field) => {
  const {
    disabled,
    input,
    meta: { invalid, touched, error },
    label,
    type,
  } = field;
  return (
    <div className="form__input">
      <TextField
        fullWidth
        type={type}
        placeholder={label}
        disabled={disabled}
        label={label}
        error={touched && invalid}
        {...input}
      />
      <div className="form__error">{touched && error}</div>
    </div>
  );
};
