import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { updateUser, getUser, resetMessageUsers } from '../actions';
import requireAuth from './hoc/requireAuth';
import FormField from './FormField';

class UserEdit extends Component {
  componentDidMount() {
    const { getUser, match, resetMessageUsers } = this.props;
    resetMessageUsers();
    getUser(match.params.id);
  }

  onSubmit(formProps) {
    const { updateUser, resetMessageUsers } = this.props;
    resetMessageUsers();
    return updateUser(formProps);
  }

  render() {
    const {
      handleSubmit, error, invalid, submitting, message,
    } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="form">
        <Field
          label="Name"
          name="fullName"
          type="text"
          component={FormField}
        />
        <Field
          disabled
          label="Role"
          name="role"
          type="text"
          component={FormField}
        />
        <Field
          disabled
          label="Email"
          name="email"
          type="text"
          component={FormField}
        />
        <Field
          label="Password"
          name="password"
          type="password"
          component={FormField}
        />
        <div className="form__button">
          <Button variant="contained" type="submit" color="primary" disabled={invalid || submitting}>
            Update
          </Button>
          { error }
          { message }
        </div>
      </form>
    );
  }
}

UserEdit.defaultProps = {
  error: '',
  message: '',
};

UserEdit.propTypes = {
  updateUser: PropTypes.func.isRequired,
  getUser: PropTypes.func.isRequired,
  resetMessageUsers: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  error: PropTypes.string,
  message: PropTypes.string,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

function validate(values) {
  const errors = {};

  if (!values.fullName) {
    errors.fullName = 'Enter a name';
  }

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.email) {
    errors.email = 'Enter a email';
  }

  return errors;
}

function mapStateToProps(state) {
  return { initialValues: state.users.user, message: state.users.message };
}

export default compose(
  connect(mapStateToProps, { updateUser, getUser, resetMessageUsers }),
  reduxForm({ validate, form: 'editUser', enableReinitialize: true }),
  requireAuth,
)(UserEdit);
