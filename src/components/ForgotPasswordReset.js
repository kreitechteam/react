import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { forgotPassword, resetAuthMessages } from '../actions';
import FormField from './FormField';
import unauthorizedOnly from './hoc/unauthorizedOnly';

class ForgotPasswordReset extends Component {
  componentWillMount() {
    const { resetAuthMessages } = this.props;
    resetAuthMessages();
  }

  onSubmit(formProps) {
    const { forgotPassword, match, resetAuthMessages } = this.props;
    resetAuthMessages();
    return forgotPassword({ ...formProps, token: match.params.token });
  }

  renderText() {
    const { error, message } = this.props;
    return (
      <div className="marginBottomMedium" style={{ height: '25px' }}>
        {error && <div className="erroredText">{ error }</div> }
        {message && <div>{ message }</div>}
      </div>
    );
  }

  render() {
    const {
      handleSubmit, invalid, submitting, pristine,
    } = this.props;

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="form">
          <span className="colorGrey">Enter a new password</span>
          <Field
            label="Password"
            name="password"
            type="password"
            component={FormField}
          />
          <span className="colorGrey">Please repeat your password</span>
          <Field
            label="Password Repeat"
            name="passwordRepeat"
            type="password"
            component={FormField}
          />
          <div className="form__button">
            {this.renderText()}
            <Button variant="contained" type="submit" color="primary" disabled={invalid || submitting || pristine}>
              SUBMIT
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

ForgotPasswordReset.propTypes = {
  forgotPassword: PropTypes.func.isRequired,
  resetAuthMessages: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  error: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
};

function validate(values) {
  const errors = {};

  if (!values.password) {
    errors.password = 'Enter a password';
  }

  if (values.password !== values.passwordRepeat) {
    errors.passwordRepeat = 'Both passwords must be the same';
  }

  return errors;
}

function mapStateToProps(state) {
  const { error, message } = state.auth;
  return { error, message };
}

export default compose(
  reduxForm({ validate, form: 'resetPassword' }),
  connect(mapStateToProps, { forgotPassword, resetAuthMessages }),
  unauthorizedOnly,
)(ForgotPasswordReset);
