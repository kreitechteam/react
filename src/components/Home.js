import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import requireAuth from './hoc/requireAuth';

function Home({ authenticated }) {
  return (
    <div style={{
      textAlign: 'center', marginTop: '10rem', padding: '2rem', wordWrap: 'break-word',
    }}
    >
      <h1>Home Page</h1>
      {authenticated}
    </div>
  );
}

Home.defaultProps = {
  authenticated: '',
};

Home.propTypes = {
  authenticated: PropTypes.string,
};

function mapStateToProps(state) {
  return { authenticated: state.auth.authenticated };
}

export default requireAuth(connect(mapStateToProps)(Home));
