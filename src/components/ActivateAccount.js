import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { resetAuthMessages, activateAccount } from '../actions';

export class ActivateAccount extends Component {
  componentWillMount() {
    const { resetAuthMessages, activateAccount, match } = this.props;
    resetAuthMessages();
    activateAccount({ token: match.params.token });
  }

  render() {
    const { error, message } = this.props;
    return (
      <div className="activateAccount">
        <div className="activateAccount__text">
          {error && <div className="activateAccount__text-errored">{ error }</div> }
          {message && <div>{ message }</div>}
        </div>
      </div>
    );
  }
}

ActivateAccount.propTypes = {
  resetAuthMessages: PropTypes.func.isRequired,
  activateAccount: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  message: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  const { message, error } = state.auth;
  return { message, error };
}

export default connect(mapStateToProps, { resetAuthMessages, activateAccount })(ActivateAccount);
