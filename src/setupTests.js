/* eslint-disable */

// Test configuration file
// localStorage and console is mocked

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn(),
  removeItem: jest.fn(),
};
global.localStorage = localStorageMock;

global.console = {
  warn: jest.fn(),
  log: jest.fn(),
  error: jest.fn(),
};
