import moxios from 'moxios';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import api from '../../services/api';

import {
  signup,
  signin,
  signout,
  facebookSignIn,
  googleSignIn,
  activateAccount,
  forgotPasswordRequest,
  forgotPassword,
} from '..';
import {
  AUTH_ERROR, LOGIN, LOGOUT, CHANGE_AUTH_MESSAGE,
} from '../types';

const mockStore = configureStore([thunk]);
let store;


const token = 'SIGN IN TOKEN';


beforeEach(() => {
  moxios.install(api.instance);
  store = mockStore();
});

afterEach(() => {
  moxios.uninstall();
});

describe('signup', () => {
  let values;

  beforeEach(() => {
    values = { username: 'username', password: 'password' };
  });

  it('has the correct type', (done) => {
    moxios.stubRequest('/signup', {
      status: 200,
      response: { token },
    });
    store.dispatch(signup(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(CHANGE_AUTH_MESSAGE);
      done();
    });
  });

  it('has the correct payload', (done) => {
    moxios.stubRequest('/signup', {
      status: 200,
      response: { token },
    });
    store.dispatch(signup(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual('Please check your email to activate your account');
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest('/signup', {
      status: 400,
      response: { token },
    });
    store.dispatch(signup(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(AUTH_ERROR);
      done();
    });
  });
});

describe('forgot password request', () => {
  let values;

  beforeEach(() => {
    values = { email: 'email' };
  });

  it('has the correct type', (done) => {
    moxios.stubRequest('/auth/get_reset_token', {
      status: 200,
      response: { token },
    });
    store.dispatch(forgotPasswordRequest(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(CHANGE_AUTH_MESSAGE);
      done();
    });
  });

  it('has the correct payload', (done) => {
    moxios.stubRequest('/auth/get_reset_token', {
      status: 200,
      response: { token },
    });
    store.dispatch(forgotPasswordRequest(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual('An email with further instructions has been sent');
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest('/auth/get_reset_token', {
      status: 400,
      response: { error: 'error' },
    });
    store.dispatch(forgotPasswordRequest(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(AUTH_ERROR);
      done();
    });
  });
});

describe('forgot password recover', () => {
  let values;

  beforeEach(() => {
    values = { password: 'password' };
  });

  it('has the correct type', (done) => {
    moxios.stubRequest('/auth/use_reset_token', {
      status: 200,
      response: { token },
    });
    store.dispatch(forgotPassword(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(LOGIN);
      done();
    });
  });

  it('has the correct payload', (done) => {
    moxios.stubRequest('/auth/use_reset_token', {
      status: 200,
      response: { token },
    });
    store.dispatch(forgotPassword(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual({ token });
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest('/auth/use_reset_token', {
      status: 400,
      response: { error: 'error' },
    });
    store.dispatch(forgotPassword(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(AUTH_ERROR);
      done();
    });
  });
});

describe('activate account', () => {
  let values;

  beforeEach(() => {
    values = { token: 'token' };
  });

  it('has the correct type', (done) => {
    moxios.stubRequest('/auth/activate', {
      status: 200,
      response: { token },
    });
    store.dispatch(activateAccount(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(CHANGE_AUTH_MESSAGE);
      done();
    });
  });

  it('has the correct payload', (done) => {
    moxios.stubRequest('/auth/activate', {
      status: 200,
      response: { token },
    });
    store.dispatch(activateAccount(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual('Your account has been activated, you can now sign in');
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest('/auth/activate', {
      status: 400,
      response: { error: 'error' },
    });
    store.dispatch(activateAccount(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(AUTH_ERROR);
      done();
    });
  });
});

describe('signout', () => {
  it('has the correct type', () => {
    const action = signout();
    expect(action.type).toEqual(LOGOUT);
  });
});

describe('signin', () => {
  let values;

  beforeEach(() => {
    values = { username: 'username', password: 'password' };
  });

  it('has the correct type', (done) => {
    moxios.stubRequest('/signin', {
      status: 200,
      response: { token },
    });
    store.dispatch(signin(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(LOGIN);
      done();
    });
  });


  it('has the correct payload', (done) => {
    moxios.stubRequest('/signin', {
      status: 200,
      response: { token },
    });
    store.dispatch(signin(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual({ token });
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest('/signin', {
      status: 400,
      response: { error: 'error' },
    });
    store.dispatch(signin(values)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(AUTH_ERROR);
      done();
    });
  });
});

describe('oauth', () => {
  const values = { accessToken: 'access token' };
  describe('facebook login', () => {
    it('should have the correct payload', (done) => {
      moxios.stubRequest('/auth/facebook', {
        status: 200,
        response: { token },
      });
      store.dispatch(facebookSignIn(values)).then(() => {
        const actions = store.getActions();
        expect(actions[0].payload).toEqual({ token });
        done();
      });
    });

    it('dispatches error', (done) => {
      moxios.stubRequest('/auth/facebook', {
        status: 400,
        response: { error: 'error' },
      });
      store.dispatch(facebookSignIn(values)).then(() => {
        const actions = store.getActions();
        expect(actions[0].type).toEqual(AUTH_ERROR);
        done();
      });
    });
  });

  describe('google login', () => {
    it('should have the correct payload', (done) => {
      moxios.stubRequest('/auth/google', {
        status: 200,
        response: { token },
      });
      store.dispatch(googleSignIn(values)).then(() => {
        const actions = store.getActions();
        expect(actions[0].payload).toEqual({ token });
        done();
      });
    });

    it('dispatches error', (done) => {
      moxios.stubRequest('/auth/google', {
        status: 400,
        response: { error: 'error' },
      });
      store.dispatch(googleSignIn(values)).then(() => {
        const actions = store.getActions();
        expect(actions[0].type).toEqual(AUTH_ERROR);
        done();
      });
    });
  });
});
