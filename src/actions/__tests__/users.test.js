import moxios from 'moxios';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import api from '../../services/api';

import { getUsers, getUser, updateUser } from '..';
import {
  GET_USERS, GET_USER, USERS_ERROR, UPDATE_USER, RESET_USERS,
} from '../types';

const mockStore = configureStore([thunk]);
let store;

const user1 = {
  role: 'user', _id: 'user_id1', email: 'user1@email.com', fullName: 'user1',
};
const user2 = {
  role: 'user', _id: 'user_id2', email: 'user2@email.com', fullName: 'user2',
};
const users = {
  users: [user1, user2,
  ],
};


beforeEach(() => {
  moxios.install(api.instance);
  store = mockStore();
});

afterEach(() => {
  moxios.uninstall();
});

describe('getUsers', () => {
  it('sends reset users', (done) => {
    moxios.stubRequest('/users?page=1', {
      status: 200,
      response: { users },
    });
    store.dispatch(getUsers(1)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(RESET_USERS);
      done();
    });
  });

  it('has the correct type', (done) => {
    moxios.stubRequest('/users?page=2', {
      status: 200,
      response: { users },
    });
    store.dispatch(getUsers(2)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(GET_USERS);
      done();
    });
  });

  it('has the correct payload', (done) => {
    moxios.stubRequest('/users?page=2', {
      status: 200,
      response: { data: users },
    });
    store.dispatch(getUsers(2)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual({ data: users });
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest('/users?page=2', {
      status: 400,
    });
    store.dispatch(getUsers(2)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(USERS_ERROR);
      done();
    });
  });
});

describe('getUser', () => {
  it('has the correct type', (done) => {
    moxios.stubRequest(`/users/${user1._id}`, {
      status: 200,
      response: { user: user1 },
    });
    store.dispatch(getUser(user1._id)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(GET_USER);
      done();
    });
  });


  it('has the correct payload', (done) => {
    moxios.stubRequest(`/users/${user1._id}`, {
      status: 200,
      response: { user: user1 },
    });
    store.dispatch(getUser(user1._id)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual(user1);
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest(`/users/${user1._id}`, {
      status: 400,
    });
    store.dispatch(getUser(user1._id)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(USERS_ERROR);
      done();
    });
  });
});

describe('updateUser', () => {
  const newValues = {
    _id: user1._id, fullName: 'new name', password: 'new_password', role: 'new role',
  };
  it('has the correct type', (done) => {
    moxios.stubRequest(`/users/${user1._id}`, {
      status: 200,
      response: { ...newValues, email: user1.email },
    });
    store.dispatch(updateUser(newValues)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(UPDATE_USER);
      done();
    });
  });


  it('has the correct payload', (done) => {
    const { fullName, role } = newValues;
    moxios.stubRequest(`/users/${user1._id}`, {
      status: 200,
      response: { fullName, role, email: user1.email },
    });
    store.dispatch(updateUser(newValues)).then(() => {
      const actions = store.getActions();
      expect(actions[0].payload).toEqual({ fullName, role, email: user1.email });
      done();
    });
  });

  it('dispatches error', (done) => {
    moxios.stubRequest(`/users/${user1._id}`, {
      status: 400,
    });
    store.dispatch(updateUser(newValues)).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(USERS_ERROR);
      done();
    });
  });
});
