import api from '../services/api';
import {
  GET_USERS, GET_USER, USERS_ERROR, UPDATE_USER, RESET_USERS, RESET_MESSAGE_USERS,
} from './types';

export const getUsers = page => dispatch => api.get('/users', { page }).then((response) => {
  if (page === 1) {
    dispatch({ type: RESET_USERS });
  }
  dispatch({
    type: GET_USERS,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: USERS_ERROR,
    payload: error,
  });
});

export const resetMessageUsers = () => ({ type: RESET_MESSAGE_USERS });

export const getUser = id => dispatch => api.get(`/users/${id}`).then((response) => {
  dispatch({
    type: GET_USER,
    payload: response.data.user,
  });
}).catch((error) => {
  dispatch({
    type: USERS_ERROR,
    payload: error,
  });
});


export const updateUser = values => dispatch => api.put(`/users/${values._id}`, values).then((response) => {
  dispatch({
    type: UPDATE_USER,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: USERS_ERROR,
    payload: error,
  });
});
