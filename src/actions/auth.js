import api from '../services/api';
import {
  AUTH_ERROR, LOGOUT, LOGIN, CHANGE_AUTH_MESSAGE, AUTH_RESET_MESSAGES,
} from './types';

export const activateAccount = values => dispatch => api.post('/auth/activate', values).then((response) => {
  dispatch({
    type: CHANGE_AUTH_MESSAGE,
    payload: 'Your account has been activated, you can now sign in',
  });
  dispatch({
    type: LOGIN,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const signup = values => dispatch => api.post('/signup', values).then(() => {
  dispatch({
    type: CHANGE_AUTH_MESSAGE,
    payload: 'Please check your email to activate your account',
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const signin = values => dispatch => api.post('/signin', values).then((response) => {
  dispatch({
    type: LOGIN,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const forgotPassword = values => dispatch => api.post('/auth/use_reset_token', values).then((response) => {
  dispatch({
    type: LOGIN,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const forgotPasswordRequest = values => dispatch => api.post('/auth/get_reset_token', values).then(() => {
  dispatch({
    type: CHANGE_AUTH_MESSAGE,
    payload: 'An email with further instructions has been sent',
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const facebookSignIn = values => dispatch => api.post('/auth/facebook', { ...values, access_token: values.accessToken }).then((response) => {
  // token must be send in key called access_token, if using passport-facebook-token
  // package in a node backend server
  // change according to backend server
  dispatch({
    type: LOGIN,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const googleSignIn = values => dispatch => api.post('/auth/google', { ...values, access_token: values.accessToken }).then((response) => {
  // token must be send in key called access_token, if using passport-google-token
  // package in a node backend server
  // change according to backend server
  dispatch({
    type: LOGIN,
    payload: response.data,
  });
}).catch((error) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.response.data.error,
  });
});

export const resetAuthMessages = () => ({ type: AUTH_RESET_MESSAGES });

export const signout = () => ({ type: LOGOUT });
