// export const CHANGE_AUTH = 'CHANGE_AUTH';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const AUTH_ERROR = 'AUTH_ERROR';
export const CHANGE_AUTH_MESSAGE = 'CHANGE_AUTH_MESSAGE';
export const AUTH_RESET_MESSAGES = 'AUTH_RESET_MESSAGES';

export const GET_USERS = 'GET_USERS';
export const GET_USER = 'GET_USER';
export const USERS_ERROR = 'USERS_ERROR';
export const UPDATE_USER = 'UPDATE_USER';
export const RESET_USERS = 'RESET_USERS';
export const RESET_MESSAGE_USERS = 'RESET_MESSAGE_USERS';
