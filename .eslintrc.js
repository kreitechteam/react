module.exports = {
    "extends": "airbnb",
    "rules": {
      "react/jsx-filename-extension": 0,
      "class-methods-use-this": 0,
      "no-shadow": 0,
      "jsx-a11y/label-has-for": 0,
      "no-underscore-dangle": 0,
      "react/forbid-prop-types": 0,
      "jsx-a11y/interactive-supports-focus": 0,
      "jsx-a11y/click-events-have-key-events": 0,
      "import/no-named-as-default": 0,
    },
    "env": {
      "jest": true,
      "browser": true
    }
};
